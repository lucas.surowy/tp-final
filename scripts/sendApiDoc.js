const axios = require('axios').default;

const {
    SLACK_WEBHOOK: sw,
    SLACK_CHANNEL: sc,
    SLACK_USERNAME: su,
    SSH_SERVER_IP: serverIp,
    SSH_PORT_WEB: port,
    SSH_PROJECT_NAME: projectName,
    CI_COMMIT_BRANCH: branchName,
    CI_COMMIT_TAG: tagName
} = process.env,
    version = typeof branchName == "undefined" ? tagName : "latest";

console.log("VERSION => ", version)

axios.post(sw, {
    channel: sc,
    username: su,
    blocks: [
		{
			"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": `La mise en prod est terminée ! Version : ${version}` 
			},
			"accessory": {
				"type": "button",
				"text": {
					"type": "plain_text",
					"text": "Consulter la doc",
					"emoji": true
				},
				"value": "click_me",
				"url": `http://${serverIp}:${port}/${projectName}/prod/docs/index.html`,
				"action_id": "button-action"
			}
		}
	]
}).then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });