const axios = require('axios').default;

const {
    SLACK_WEBHOOK: sw,
    SLACK_CHANNEL: sc,
    SLACK_USERNAME: su,
    SSH_SERVER_IP: serverIp,
    SSH_PORT_WEB: port,
    SSH_PROJECT_NAME: projectName,
    CI_COMMIT_SHORT_SHA: commitSha
} = process.env;

axios.post(sw, {
    channel: sc,
    username: su,
    blocks: [
		{
			"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": "OH MON DIEUX !!! Une erreur sur les test ! "
			},
			"accessory": {
				"type": "button",
				"text": {
					"type": "plain_text",
					"text": "Consulter le rapport",
					"emoji": true
				},
				"value": "click_me",
				"url": `http://${serverIp}:${port}/${projectName}/dev/test-report-${commitSha}.html`,
				"action_id": "button-action"
			}
		}
	]
}).then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });